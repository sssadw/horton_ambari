class horton_ambari::defaults  {
  $nopen = 10000

  $ssh_user = 'ambari'
  $ssh_name = 'ambari@sseambari'
  $ssh_type = 'ssh-rsa' 
  $ssh_key  = 'AAAAB3NzaC1yc2EAAAABIwAAAQEAvGhVkThmnsbXuxGZh1o+N8slp0dXlBoZGYQzt8bK7Cmbo7bosTY3tMRK2FRxxsQ9cqDHWz8HuOBYrkdx0gGHLXZS7QuZkureWh93W2LVTEzr2NL6yWOTDOPDI5/hWldWNml2JpB0UulZl+A9Rbc2XwuJj1hJd1qX+/5DETPMnp2skSJGBHTI4XfvcxBMdnDXbcJEgzaQvprgVBNoUJ25pdp5o5nTAHji3ri5D9xHWIjdb/9KRDDUix0Sgj0m4pGb2sZQJCtULuJmDWDPfIrVVurffftVlHfnnKODEcC0RTmA2h6L4eBx8V9EQChQX4Vt3SYl1clNWYdul1zJ7zg1dw=='
  case $horton_ambari::version {
    '1.7.0': {
      case $::osfamily {
        'Debian': {
          $apt_comment  = 'http://public-repo-1.hortonworks.com/ambari/ubuntu12/1.x/updates/1.7.0/ambari.list'
          $apt_location = 'http://public-repo-1.hortonworks.com/ambari/ubuntu12/1.x/updates/1.7.0'
          $apt_key	= 'B9733A7A07513CAD'
        }
        'Redhat': {
          $yum_baseurl = 'http://public-repo-1.hortonworks.com/ambari/centos6/1.x/updates/1.7.0'
          $yum_gpgkey  = 'http://public-repo-1.hortonworks.com/ambari/centos6/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    '2.0.0': {
      case $::osfamily {
        'Debian': {
          $apt_comment  = 'http://public-repo-1.hortonworks.com/ambari/ubuntu12/2.x/updates/2.0.0/ambari.list'
          $apt_location = 'http://public-repo-1.hortonworks.com/ambari/ubuntu12/2.x/updates/2.0.0'
          $apt_key	= 'B9733A7A07513CAD'
        }
        'Redhat': {
          $yum_baseurl = 'http://public-repo-1.hortonworks.com/ambari/centos6/2.x/updates/2.0.0'
          $yum_gpgkey  = 'http://public-repo-1.hortonworks.com/ambari/centos6/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    '2.1.1': {
      case $::osfamily {
        'Redhat': {
          $yum_baseurl = 'http://public-repo-1.hortonworks.com/ambari/centos6/2.x/updates/2.1.1'
          $yum_gpgkey  = 'http://public-repo-1.hortonworks.com/ambari/centos6/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    '2.1.2': {
      case $::osfamily {
        'Redhat': {
          $yum_baseurl = 'http://public-repo-1.hortonworks.com/ambari/centos6/2.x/updates/2.1.2'
          $yum_gpgkey  = 'http://public-repo-1.hortonworks.com/ambari/centos6/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    '2.4.1': {
      case $::osfamily {
        'Redhat': {
          $centos = "centos${operatingsystemmajrelease}"
          $ambari_url  = 'http://public-repo-1.hortonworks.com/ambari'
          $yum_baseurl = "${ambari_url}/${centos}/2.x/updates/2.4.1.0"
          $yum_gpgkey  = "${ambari_url}/${centos}/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins"
#          $yum_baseurl = 'http://public-repo-1.hortonworks.com/ambari/centos7/2.x/updates/2.4.1.0'
#          $yum_gpgkey  = 'http://public-repo-1.hortonworks.com/ambari/centos7/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    '2.5.1': {
      case $::osfamily {
        'Redhat': {
          $centos = "centos${operatingsystemmajrelease}"
          $ambari_url  = 'http://public-repo-1.hortonworks.com/ambari'
          $yum_baseurl = "${ambari_url}/${centos}/2.x/updates/2.5.1.0"
          $yum_gpgkey  = "${ambari_url}/${centos}/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins"
        }
        'Debian': {
          $apt_comment  = 'http://public-repo-1.hortonworks.com/ambari/ubuntu16/2.x/updates/2.5.1.0/ambari.list'
          $apt_location = 'http://public-repo-1.hortonworks.com/ambari/ubuntu16/2.x/updates/2.5.1.0'
          $apt_key	= 'B9733A7A07513CAD'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    '2.5.2': {
      case $::osfamily {
        'Redhat': {
          $centos = "centos${operatingsystemmajrelease}"
          $ambari_url  = 'http://public-repo-1.hortonworks.com/ambari'
          $yum_baseurl = "${ambari_url}/${centos}/2.x/updates/2.5.2.0"
          $yum_gpgkey  = "${ambari_url}/${centos}/RPM-GPG-KEY/RPM-GPG-KEY-Jenkins"
        }
        'Debian': {
          $apt_comment  = 'http://public-repo-1.hortonworks.com/ambari/ubuntu16/2.x/updates/2.5.2.0/ambari.list'
          $apt_location = 'http://public-repo-1.hortonworks.com/ambari/ubuntu16/2.x/updates/2.5.2.0'
          $apt_key	= 'B9733A7A07513CAD'
        }
        default: {
          fail("Unsuported OSfamily $::osfamily")
        }
      }
    }
    default: {
	fail("unknown version $version")
    }
  }

}
