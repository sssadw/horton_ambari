# local users needed for cdm

class horton_ambari::users (
  String $ensure = 'present',
  Array $extra_users = ['zookeeper', 'oozie', 'hbase', 'hue', 'sqoop', 
                        'httpfs', 'solr', 'spark', 'kms'],
) {
  Accounts::User {
    ensure => $ensure,
    password => '!!',
  }
  accounts::user { 'hadoop': }
  accounts::user { 'hdfs':   groups => [ 'hadoop'], require => Accounts::User['hadoop']}
  accounts::user { 'yarn':   groups => [ 'hadoop'], require => Accounts::User['hadoop']}
  accounts::user { 'mapred': groups => [ 'hadoop'], require => Accounts::User['hadoop']}
  accounts::user { 'hive': }
  accounts::user { 'impala': groups => [ 'hive'],   require => Accounts::User['hive']}
  accounts::user { $extra_users: }
}
  
