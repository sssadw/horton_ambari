class horton_ambari::server (
  String  $version,
  String  $server_ensure = 'running',
  Boolean $server_enable = true,
) {
#  require user_admin::research_server
  class { 'horton_ambari': version => $version } ->
  class { 'horton_ambari::server_setup': 
    version => $version,
    server_ensure => $server_ensure,
    server_enable => $server_enable,
  }
}
