#

class horton_ambari::set_hostname (
  Array $reloads = [],
) {
  class { 'hostname':
    hostname => $facts['hostname'].split('\.')[1],
    domain   => $facts['networking']['domain'],
    ip       => $facts['ipaddress'],
    reloads  => $reloads,
  }
  file_line { 'localhost1':
    ensure => absent,
    path     => '/etc/hosts',
    line     => '127.0.1.1 localhost1',
    match    => '^127.0.1.1',
    match_for_absence => true,
  }
}
