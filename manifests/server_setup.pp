class horton_ambari::server_setup (
  String  $version,
  String  $server_ensure='running',
  Boolean $server_enable=true,
) {
#require user_admin::research_server

  package { ambari-server:
    ensure => latest,
  }

  if ( $horton_ambari::version == '2.4.1' ) {
    file_line { 'ambari-server-2.4.1-patch':
      path    => '/etc/init.d/ambari-server',
      line    => 'export ROOT=\'/\'',
      after   => 'ROOT=`echo $ROOT | sed \'s/\\/$//\'`',
      require => Package['ambari-server'],
#      before  => Service['ambari-server'],
    }
  }
     
#  service { ambari-server:
#    ensure => $server_ensure,
#    enable => $server_enable,
#    require => Package['ambari-server'],
#  }

#   file { '/home/itng/.ssh':
#        ensure => directory,
#        mode => '0700',
#   }
#
#   file {'/home/itng/.ssh/id_rsa':
#        source => 'puppet:///modules/horton_ambari/id_rsa',
#        owner => 'itng',
#        mode => '0600',
#        require => File['/home/itng/.ssh'],
#   }
#
#   file {'/home/itng/.ssh/id_rsa.pub':
#        source => 'puppet:///modules/horton_ambari/id_rsa.pub',
#        owner => 'itng',
#        mode => '0644',
#        require => File['/home/itng/.ssh'],
#   }

}
