class horton_ambari (
   String  $version,
   Integer $nopen,
   String  $ssh_user,
   String  $ssh_name,
   String  $ssh_type,
   String  $ssh_key,
) {

  user { $ssh_user:
    ensure => present,
    managehome => true,
  }
  sudo::conf { 'ambarinopw': content => "${ssh_user} ALL=(ALL) NOPASSWD:ALL", }
  # maximum number of open files/sockets for all 
  $nopen_str = "${nopen}"
  limits::fragment { 
    "default-soft": domain => '*',  type => soft, item => nofile, value => $nopen_str;
    "default-hard": domain => '*',  type => hard, item => nofile, value => $nopen_str;
    "root-soft":    domain => root, type => soft, item => nofile, value => $nopen_str;
    "root-hard":    domain => root, type => hard, item => nofile, value => $nopen_str;

  }
  ssh_authorized_key { "default-ssh-key-for-ambari": 
    user   => $ssh_user,
    ensure => present, 
    type   => $ssh_type,
    key    => $ssh_key,
    name   => $ssh_name,
    require => User[ $ssh_user ],
  }
  package { 'mysql-connector-java': ensure =>installed }
  case $osfamily {
    'Debian': {
      service { "ufw":
        ensure => 'stopped',
        enable => false,
      }

      apt::source { 'ambari':
        name        => 'ambaris',
        comment     => lookup("horton_ambari::apt_comment::${version}"),
        location    => lookup("horton_ambari::apt_location::${version}"),
        release     => 'Ambari',
        repos	      => 'main',
        key         => lookup("horton_ambari::apt_key::${version}"),
        include_src => false,
      }
    }
    'Redhat': {
      service { 'iptables': ensure => 'stopped', enable => false, }
      service { 'firewalld': ensure => 'stopped', enable => false, }
      file_line { 'selinux':
        path  => '/etc/selinux/config',
        line  => 'SELINUX=disabled',
        match => '^SELINUX=',
      }
      $repofile=lookup("horton_ambari::yum_repofile::${version}",undef,undef,false)
      #install repofile for web
      if $repofile {
        file {'/etc/yum.repos.d/ambari.repo':
          source => $repofile,
        }
      } else {
        yumrepo { 'ambari':
          name     => 'ambari',
          baseurl  => lookup("horton_ambari::yum_baseurl::${version}"),
          descr    => 'Ambari Repo',
          enabled  => 1,
          gpgcheck => 1,
          gpgkey   => lookup("horton_ambari::yum_gpgkey"),
        }
      }
      exec {'disable_thp':
        command => '/bin/echo never > /sys/kernel/mm/transparent_hugepage/enabled',
        unless => '/bin/grep -c "\[never]" /sys/kernel/mm/transparent_hugepage/enabled',
      }
    } 
    default: {
    }
  } 
}
