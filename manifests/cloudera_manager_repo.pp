#

class horton_ambari::cloudera_manager_repo() 
{

  file { '/etc/apt/sources.list.d/cloudera-manager1.list':
    ensure => 'present',
    content => 'deb [arch=amd64] https://archive.cloudera.com/cm7/7.4.4/ubuntu2004/apt focal-cm7.4.4 contrib'
  }
}
