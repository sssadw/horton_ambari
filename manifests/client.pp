class horton_ambari::client (
   String $version,
   String $agent_ensure='running',
   String $agent_enable='true',
   String $agent_server='false',
   String $ensure = 'installed',
   String $ssh_user = 'ambari',
   String $ssh_name = 'ambari@cs-ambari',
   String $ssh_type = 'ssh-rsa',
   String $ssh_key,  
   String $java_package = 'java-1.8.0-openjdk',
   String $hdp_version = '3.1.4.0-315',
) {
#  require user_admin::research_desktop
  
  if $ensure == 'installed' {
    class { 'horton_ambari':
      version  => $version,
      ssh_user => $ssh_user,
      ssh_name => $ssh_name,
      ssh_type => $ssh_type,
      ssh_key  => $ssh_key,
    }
    ->
    package { 'ambari-agent':
      ensure => latest
    }
    package { $java_package: ensure => 'installed' }
    service { 'ambari-agent':
      ensure => $agent_ensure,
      enable => $agent_enable,
      require => Package['ambari-agent'],
    }
    unless ( $agent_server == 'false' ) {
      file_line { 'ambari-agent-server':
        path    => '/etc/ambari-agent/conf/ambari-agent.ini',
        line    => "hostname=${agent_server}",
        match   => 'hostname',
        require => Package['ambari-agent'],
        before  => Service['ambari-agent'],
        notify  => Service['ambari-agent'],
      }
    }
     
#legacy to make it look like CDH
  file { '/usr/lib/hadoop':
    ensure => 'directory',
  }
  file { '/usr/lib/hadoop/client':
   ensure => 'link',
   target => "/usr/hdp/${hdp_version}/hadoop/client",
   require => File['/usr/lib/hadoop'],
  }
#  file { '/etc/security/limits.d/kafka.conf':
#    source => 'puppet:///modules/horton_ambari/limits.d/kafka.conf',
#  }
#  file { '/var/hadoop': 
#    ensure => 'directory' 
#  }
#  file { '/var/hadoop/hdfs': 
#    ensure => 'directory', 
#    require => File['/var/hadoop'],
#  }
#  file { ['/var/hadoop/hdfs/data', '/var/hadoop/hdfs/namenode']: 
#    ensure => 'directory', 
#    require => File['/var/hadoop/hdfs'],
#    owner => 'hdfs',
#    group => 'hadoop',
#  }
  }
}
