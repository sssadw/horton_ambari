class horton_ambari::upgrade_metrics (
) {
#  require user_admin::research_desktop
  package { 'ambari-metrics-monitor': ensure => latest }
  package { 'ambari-metrics-hadoop-sink': ensure => latest }
#  package { 'ambari-log4j': ensure => latest }
}
