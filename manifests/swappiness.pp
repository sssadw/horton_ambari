# swappiness needed for cdm

class horton_ambari::swappiness (
  String  $ensure = 'present',
  Integer $value  = 10,
) {
  sysctl { "vm.swappiness":
    ensure => $ensure,
    value  => "${value}",
  }
}
  
